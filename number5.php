<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <title>Number 5</title>
</head>
<body>
    <h3>Write a program to delete the recurring elements inside a sorted list of integers. </h3>

    <p>  BEFORE: 1, 1, 2, 2, 3, 4, 5, 5</p>

    <?php
    $nums = array(1,1,2,2,3,4,5,5);

    function remove_duplicates_list($list1) {
  $nums_unique = array_values(array_unique($list1));
  return $nums_unique ;
}
    echo "AFTER: " ;
    print_r(remove_duplicates_list($nums));
?>
</body>
</html>