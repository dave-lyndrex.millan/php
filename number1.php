<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Number 1</title>
</head>
<body>
<div class="container p-3 my-5 bg-white text-dark" >
<center>
<h3>Write a program to loop over the given JSON data. Display the values via loops or recursion.
</h3>

<br>

<?php

$data = array(
"Student1" => array(
"Name" => "John Carg",
"Age" => "17",
"School" => "Ahlcon Public school",
),
"Student2" => array(
"Name" => "Smith Soy",
"Age" => "16",
"School" => "St. Marie school",
),
"Student3" => array(
"Name" => "Charle Rena ",
"Age" => "16",
"School" => "St. Columba school",
)
);
$key1 = array_keys($data);
for($ctr = 0; $ctr < count($data); $ctr++) {
echo $key1[$ctr] ."<br><br>";
foreach($data[$key1[$ctr]] as $key2 => $value) {
echo $key2 . " : " . $value . "<br>";
}
echo "<br><br>";
}
?>
</div>
</center> 
</body>
</html>
