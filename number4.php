<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Number 4</title>
</head>
<body>
  <center><h3>Write a program to determine if the number is an Armstrong Number. </h3> 

  <div class="container p-3 my-5 bg-white text-dark" style="width:250px; height:600px">

<hr>
<br>
<form class="form-inline" method="GET">
<div class="form-group mx-sm-3 mb-2">
<label >Input a Numbers: </label>
<input type="text" class="form-control" id="number" name="number">
</div>
<button type="submit" class="btn btn-outline-primary" name = "convert">Check</button>
</form>
<br>
<hr>
<?php
if(isset($_GET["convert"])){
$getInput = $_GET['number'];
$result = 0;
for ($ctr =0;$ctr< strlen($getInput);$ctr++){
$result += $getInput[$ctr]**3;
}
if($getInput == $result){
echo "ARMSTRONG NUMBER";
}else{
echo "NOT ARMSTRONG NUMBER";
}
}
?>
</div>
</center> 
</body>
</html>