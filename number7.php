<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Number 7</title>
</head>
<body>
<center>
<h3 style="margin-top:50px;">Write a program to convert a digit into its word counterpart. </h3>

<br>
   
    <form method = "post">
    Input Number: <input type="number" name= "num">
    <button class="btn btn-outline-primary btn-sm" name="btn">Convert</button>
    </form>

    <?php

if(isset($_POST['btn'])){
    $num = $_POST['num'];
    getLength($num);
}

function getLength($num){
    $str_length = strlen((string)$num);
    for ($ctr=0; $ctr <$str_length ; $ctr++) { 
        getNumIndex($num[$ctr]);
    }

}
function getNumIndex($digit){              
    switch($digit){
        
        case '0':
            echo "Zero ";
            break;
        case '1':
            echo "One ";
            break;
        case '2':
            echo "Two ";
            break;
        case '3':
            echo "Three ";
            break;
        case '4':
            echo "Four ";
            break;
        case '5':
            echo "Five ";
            break;
        case '6':
            echo "Six ";
            break;
        case '7':
            echo "Seven ";
            break;
        case '8':
            echo "Eight ";
            break;
        case '9':
            echo "Nine ";
            break;
                            
    }

}
?>
</center>

</body>
</html>